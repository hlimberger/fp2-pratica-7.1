/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.*;
import java.util.List;
import java.util.Iterator;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Pratica71 {
    public static void main(String[] args) {
       
        int qtde = 0, num = 1, i = 0;
        String nome, temp;
        boolean eNum = false;
        Time cam = new Time();
        Scanner sc = new Scanner(System.in);
        
        
        
        do {
            try {
                System.out.print("Digite o número de jogadores do time: ");
                qtde = sc.nextInt();
            } catch (InputMismatchException e) {
                System.out.println("AVISO: Você digitou um número inválido.");
            }
            sc.nextLine();
        } while (qtde <= 0);
        
        while(i < qtde){
            System.out.println("Digite os dados do jogador: ");
            num = -1;
            do {
                try {
                    System.out.print("Número: ");
                    num = sc.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("AVISO: Você digitou um número inválido.");
                }
                sc.nextLine();
            } while (num < 0);
            
            System.out.print("Nome: ");
            nome = sc.next();
            cam.addJogador(Integer.toString(num) , new Jogador(num,nome));
            i++;
        }
               
        List<Jogador> lista = cam.ordena(new JogadorComparator(true,true,true));
        
        Iterator<Jogador> it = lista.iterator();
        
        System.out.println("Lista de jogadores:");
        while(it.hasNext()){
            Jogador j = it.next();
            System.out.println(j.toString());
        }

        while(num != 0){
            System.out.println("Digite os dados do jogador: (0 para cancelar)");
            num = -1;
            do {
                try {
                    System.out.print("Número: ");
                    num = sc.nextInt();
                } catch (InputMismatchException e) {
                    System.out.println("AVISO: Você digitou um número inválido.");
                }
                sc.nextLine();
            } while (num < 0);
            
            if(num == 0){
                break;
            }
            
            System.out.print("Nome: ");
            nome = sc.next();
            
            cam.addJogador(Integer.toString(num) , new Jogador(num,nome));
            
            lista = cam.ordena(new JogadorComparator(true,true,true));
            
            it = lista.iterator();
            
            System.out.println("Lista de jogadores:");
             
            while(it.hasNext()){
                Jogador j = it.next();
                System.out.println(j.toString());
            }
            
        }
        sc.close();
            
       /* Jogador jogador = new Jogador(16, "Rodrigão");
        
        int index = Collections.binarySearch(lista, jogador);
        
        System.out.println("Jogador encontrado: " + index);*/
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

/**
 *
 * @author henrique
 */
public class Time {
    private HashMap<String, Jogador> jogadores;
    
    public Time(){
        this.jogadores = new HashMap<>();
    }
    
    public HashMap<String, Jogador> getJogadores(){
        return this.jogadores;
    }
    
    public void addJogador(String chave, Jogador jogador){
        this.jogadores.put(chave, jogador);
    }
    
    public List<Jogador> ordena(JogadorComparator comparador){
        List<Jogador> lista = new ArrayList<>(jogadores.values());
        lista.sort(comparador);
        return lista;
    }
}

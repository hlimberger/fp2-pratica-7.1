

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;


import java.util.Comparator;
/**
 *
 * @author henrique
 */
public class JogadorComparator implements Comparator<Jogador> {
    public boolean porNumero;
    public boolean numAsc;
    public boolean nomeAsc;
    
    public JogadorComparator(){
        this.porNumero = true;
        this.numAsc = true;
        this.nomeAsc = true;
    }
    
    public JogadorComparator(boolean porNumero, boolean asc, boolean nomeAsc){
        this.porNumero = porNumero;
        this.numAsc = asc;
        this.nomeAsc = nomeAsc;
    }
    
    @Override
    public int compare(Jogador a,Jogador b){
        int compNum, compNome, retorna;
        compNum = a.numero - b.numero;
        compNome = a.nome.compareTo(b.nome);
       
        if(numAsc != true){
            compNum *= -1;
        }
        if(nomeAsc != true){
            compNome *= -1;
        }
        
        if(porNumero == true){
            if(compNum != 0){
                retorna = compNum;
            } else {
                retorna = compNome;
            }
        } else {
            if(compNome != 0){
                retorna = compNome;
            } else {
                retorna = compNum;
            }
        }
        
        return retorna;
    }
}